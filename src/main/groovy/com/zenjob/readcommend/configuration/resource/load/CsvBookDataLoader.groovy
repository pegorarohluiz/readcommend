package com.zenjob.readcommend.configuration.resource.load

import com.zenjob.readcommend.model.Book
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ResourceLoaderAware
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.io.ResourceLoader

@Profile("data-book-csv")
@Configuration
class CsvBookDataLoader implements ResourceLoaderAware {

    private static final String BOOK_CSV_FILE_PATH = "classpath:books.csv"
    private static final Logger LOG = LoggerFactory.getLogger(CsvBookDataLoader)
    public static final String HEADER_FIRST = "ASIN"

    private ResourceLoader resourceLoader

    @Autowired
    CsvBookDataLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader
    }

    @Bean("bookData")
    Map<String, Book> bookData() {
        Map<String, Book> bookMap = new HashMap<>()
        def resource = resourceLoader.getResource(BOOK_CSV_FILE_PATH)
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()))
            bufferedReader.splitEachLine(";") { fields ->
                def book = new Book(fields[0], fields[1], fields[2], fields[3])
                if (book.asin != HEADER_FIRST) {
                    LOG.debug("Adding to db: {}", book)
                    bookMap.put(fields[0], book)
                }
            }
        } catch (IOError ioError) {
            LOG.error("Error loading book data resource", ioError)
        }
        bookMap
    }

    @Override
    void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader
    }
}

package com.zenjob.readcommend.configuration

import com.zenjob.readcommend.configuration.tenant.TenantService
import com.zenjob.readcommend.model.Tenant

interface TenantAware {
    Tenant getTenant(String tenantId)
    TenantService getTenantService()
}
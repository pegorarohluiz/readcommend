package com.zenjob.readcommend.configuration.tenant


import com.zenjob.readcommend.model.Tenant
import com.zenjob.readcommend.model.User
import com.zenjob.readcommend.model.repository.TenantRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DefaultTenantService implements TenantService {

    private final TenantRepository tenantRepository

    @Autowired
    DefaultTenantService(TenantRepository tenantRepository) {
        this.tenantRepository = tenantRepository
        createDefaultTenant()
    }

    @Override
    Tenant getCurrentTenant() {
        return tenantRepository.findById(UUID.nameUUIDFromBytes(LATEST.bytes).toString())
                .orElseGet({-> createDefaultTenant()})
    }

    @Override
    Tenant useTenant(String tenantId) {
        return tenantRepository.findById(tenantId)
                .orElseGet({ -> getCurrentTenant()})
    }

    @Override
    Tenant createDefaultTenant() {
        return tenantRepository.save(new Tenant(UUID.nameUUIDFromBytes(LATEST.bytes).toString(), new HashSet<>()))
    }

    @Override
    void addUserToTenant(User user, Tenant tenant) {
        tenant.users.add user
        tenantRepository.save(tenant)
    }
}

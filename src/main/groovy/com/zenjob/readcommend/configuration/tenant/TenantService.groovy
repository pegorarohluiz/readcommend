package com.zenjob.readcommend.configuration.tenant

import com.zenjob.readcommend.model.Tenant
import com.zenjob.readcommend.model.User

interface TenantService {

    public static final String LATEST = "latest"
    public static final String TENANT_HEADER = "X-Tenant-ID"

    Tenant getCurrentTenant()
    Tenant useTenant(String tenantId)
    Tenant createDefaultTenant()
    void addUserToTenant(User user, Tenant tenant)
}
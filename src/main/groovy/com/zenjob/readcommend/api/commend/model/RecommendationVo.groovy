package com.zenjob.readcommend.api.commend.model

import com.zenjob.readcommend.model.enums.Evaluation
import org.springframework.lang.Nullable

class RecommendationVo {
    private final String bookAisn
    private final Evaluation evaluation

    RecommendationVo(String bookAisn, @Nullable Evaluation evaluation) {
        this.bookAisn = bookAisn
        this.evaluation = evaluation
    }

    String getBookAisn() {
        return bookAisn
    }

    Evaluation getEvaluation() {
        return evaluation
    }
}

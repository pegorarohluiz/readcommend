package com.zenjob.readcommend.api.commend.controller

import com.zenjob.readcommend.api.commend.model.RecommendationVo
import com.zenjob.readcommend.api.user.service.UserService
import com.zenjob.readcommend.configuration.tenant.TenantService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import static com.zenjob.readcommend.configuration.tenant.TenantService.TENANT_HEADER

@RestController
@RequestMapping("recommendations")
class CommendController {

    private final UserService userService

    @Autowired
    CommendController(UserService userService) {
        this.userService = userService
    }

    @PostMapping("/{uid}/")
    def addNewRecommendations(
            @RequestHeader(name = TENANT_HEADER, required = false, defaultValue = TenantService.LATEST) String tenantId,
            @PathVariable String uid,
            @RequestBody Set<RecommendationVo> recommendationVos) {
        userService.saveRecommendations(tenantId, UUID.fromString(uid), recommendationVos)
        ResponseEntity.accepted()
            .build()
    }

    @GetMapping("/{username}")
    def retrieveBooksForRecommendation(
            @RequestHeader(name = TENANT_HEADER, required = false, defaultValue = TenantService.LATEST) String tenantId,
            @PathVariable String username,
            @RequestParam(required = false, defaultValue = "10") String pageSize,
            @RequestParam(required = false, defaultValue = "0") String page) {
        ResponseEntity.ok(userService.getPageListHolderOfRecommendation(tenantId, username, Integer.valueOf(pageSize).intValue(), Integer.valueOf(page).intValue()))
    }

}

package com.zenjob.readcommend.api.user.service

import com.zenjob.readcommend.api.commend.model.RecommendationVo
import com.zenjob.readcommend.model.Recommendation
import com.zenjob.readcommend.model.User
import org.springframework.beans.support.PagedListHolder

interface UserService {
    User createNewUser(String tenantId, String username)
    User getUser(String tenantId, UUID uuid)
    void saveRecommendations(String tenantId, UUID uid, Set<RecommendationVo> recommendations)
    PagedListHolder<Recommendation> getPageListHolderOfRecommendation(String tenantId, String username, int pageSize, int page)
}
package com.zenjob.readcommend.api.user.controller

import com.zenjob.readcommend.api.user.service.DefaultUserService
import com.zenjob.readcommend.api.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import static com.zenjob.readcommend.configuration.tenant.TenantService.LATEST
import static com.zenjob.readcommend.configuration.tenant.TenantService.TENANT_HEADER

@RestController
@RequestMapping("/user")
class UserController {
    private final UserService userService

    @Autowired
    UserController(DefaultUserService userService) {
        this.userService = userService
    }

    @PostMapping
    def createUser(@RequestHeader(value = TENANT_HEADER, required = false, defaultValue = LATEST) String tenantId,
                   @RequestBody String username) {
        ResponseEntity.ok userService.createNewUser(tenantId, username)
    }

    @GetMapping("/{uid}")
    def getUser(@RequestHeader(value = TENANT_HEADER, required = false, defaultValue = LATEST) String tenantId,
                @RequestParam("uid") String uid) {
        LOG.log("Getting user from ${uid} in ${tenantId} tenantId")
        ResponseEntity.ok userService.getUser(tenantId, uid)
    }

}

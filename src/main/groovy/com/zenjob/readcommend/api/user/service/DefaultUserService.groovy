package com.zenjob.readcommend.api.user.service

import com.zenjob.readcommend.api.commend.model.RecommendationVo
import com.zenjob.readcommend.configuration.TenantAware
import com.zenjob.readcommend.configuration.tenant.TenantService
import com.zenjob.readcommend.model.Book
import com.zenjob.readcommend.model.Recommendation
import com.zenjob.readcommend.model.Tenant
import com.zenjob.readcommend.model.User
import com.zenjob.readcommend.model.repository.BookRepository
import com.zenjob.readcommend.model.repository.UserRepository
import org.springframework.beans.support.PagedListHolder
import org.springframework.stereotype.Service

import java.security.InvalidParameterException

@Service
class DefaultUserService implements UserService, TenantAware {

    private final UserRepository userRepository
    private final BookRepository bookRepository
    private final TenantService tenantService

    DefaultUserService(UserRepository userRepository, BookRepository bookRepository, TenantService tenantService) {
        this.userRepository = userRepository
        this.bookRepository = bookRepository
        this.tenantService = tenantService
    }

    @Override
    User createNewUser(String tenantId, String username) {
        def uuid = UUID.nameUUIDFromBytes username.getBytes()
        def user = new User(uuid, username, new HashSet<>())
        tenantService.addUserToTenant(user, getTenant(tenantId))
        userRepository.save user
    }

    @Override
    User getUser(String tenantId, UUID uuid) {
        def tenant = getTenant(tenantId)
        if (tenant.users.find { it.uid == uuid } != null) {
            return userRepository.findById(uuid)
                    .orElseThrow({ new InvalidParameterException("User with ${uuid.toString()} in Tenant #${tenantId} not found")})
        } else {
            throw new InvalidParameterException("User with ${uuid.toString()} in Tenant #${tenantId} not found")
        }
    }

    @Override
    void saveRecommendations(String tenantId, UUID uid, Set<RecommendationVo> recommendationVos) {
        def user = getUser(tenantId, uid)
        def recommendations = new HashSet<>()
        recommendationVos.each { vo ->
            def savedBook = getBookFromAisn(vo.bookAisn)
            recommendations.add new Recommendation (savedBook, vo.evaluation)
        }
        user.recommendations.addAll recommendations
        userRepository.save user
    }

    private Book getBookFromAisn(String bookAisn) {
        def optBook = bookRepository.findByAisn(bookAisn)
        return optBook
                .orElseThrow({ new InvalidParameterException("Book #${bookAisn} not found") })
    }

    @Override
    PagedListHolder<Recommendation> getPageListHolderOfRecommendation(String tenantId, String username, int pageSize, int page) {
        def uuid = UUID.nameUUIDFromBytes username.getBytes()
        def user = getUser(tenantId, uuid)
        def booksPage = bookRepository.getBooksPerPage(pageSize, page)
        def recommendationsWithAvailable = new HashSet<Recommendation>()
        booksPage.pageList.each { book ->
            def recommendation = user.recommendations.find { it.book == book }
            if (recommendation != null) {
                recommendationsWithAvailable.add(recommendation)
            } else {
                recommendationsWithAvailable.add(new Recommendation(book, null))
            }
        }
        def recommendationPage = new PagedListHolder<Recommendation>(recommendationsWithAvailable.asList())
        recommendationPage.page = page
        recommendationPage.pageSize = pageSize
        recommendationPage
    }

    @Override
    Tenant getTenant(String tenantId) {
        def tenant = tenantService.useTenant(tenantId)
        if (tenant == null) {
            throw new InvalidParameterException("Tenant $tenantId not found")
        }
        tenant
    }

    @Override
    TenantService getTenantService() {
        return tenantService
    }
}

package com.zenjob.readcommend.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
class Book {
    @Id
    private final String asin
    private final String title
    private final String author
    private final String genre

    Book(String asin, String title, String author, String genre) {
        this.asin = asin
        this.title = title
        this.author = author
        this.genre = genre
    }

    String getAsin() {
        return asin
    }

    String getTitle() {
        return title
    }

    String getAuthor() {
        return author
    }

    String getGenre() {
        return genre
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Book book = (Book) o

        if (asin != book.asin) return false

        return true
    }

    int hashCode() {
        return asin.hashCode()
    }

    @Override
    String toString() {
        return "{" +
                "asin='" + asin + '\'' +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", genre='" + genre + '\'' +
                '}'
    }
}

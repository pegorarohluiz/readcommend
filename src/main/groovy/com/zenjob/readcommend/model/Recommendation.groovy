package com.zenjob.readcommend.model

import com.mongodb.lang.Nullable
import com.zenjob.readcommend.model.enums.Evaluation
import org.springframework.data.mongodb.core.mapping.Document

@Document
class Recommendation {
    private final Book book
    private final Evaluation evaluation

    Recommendation(Book book, @Nullable Evaluation evaluation) {
        this.book = book
        this.evaluation = evaluation
    }

    Book getBook() {
        return book
    }

    Evaluation getEvaluation() {
        return evaluation
    }


    @Override
    String toString() {
        return "{" +
                "book=" + book +
                ", evaluation=" + evaluation +
                '}'
    }
}

package com.zenjob.readcommend.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
class User {
    @Id
    private final UUID uid
    private final String username
    private final Set<Recommendation> recommendations

    User(UUID uid, String username, Set<Recommendation> recommendations) {
        this.uid = uid
        this.username = username
        this.recommendations = recommendations
    }

    UUID getUid() {
        return uid
    }

    String getUsername() {
        return username
    }

    Set<Recommendation> getRecommendations() {
        return recommendations
    }


    @Override
    String toString() {
        return "{" +
                "uid=" + uid +
                ", username='" + username + '\'' +
                ", recommendations=" + recommendations +
                '}'
    }
}

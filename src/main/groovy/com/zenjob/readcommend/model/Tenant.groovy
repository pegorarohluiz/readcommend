package com.zenjob.readcommend.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
class Tenant {
    @Id
    private final String tenantId
    private final Set<User> users

    Tenant(String tenantId, Set<User> users) {
        this.tenantId = tenantId
        this.users = users
    }

    String getTenantId() {
        return tenantId
    }

    Set<User> getUsers() {
        return users
    }


    @Override
    String toString() {
        return "{" +
                "tenantId='" + tenantId + '\'' +
                ", users=" + users +
                '}'
    }
}

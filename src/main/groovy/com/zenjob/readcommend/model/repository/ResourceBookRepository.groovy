package com.zenjob.readcommend.model.repository

import com.zenjob.readcommend.model.Book
import com.zenjob.readcommend.model.repository.BookRepository
import org.springframework.beans.support.PagedListHolder
import org.springframework.stereotype.Component

import javax.annotation.Resource

@Component
class ResourceBookRepository implements BookRepository {

    @Resource(name = "bookData")
    private Map<String, Book> bookMap

    @Override
    Optional<Book> findByAisn(String ISN) {
        bookMap.containsKey(ISN) ? Optional.of(bookMap.get(ISN)) : Optional.empty() as Optional<Book>
    }

    @Override
    PagedListHolder<Book> getBooksPerPage(int size, int page) {
        def bookPagedListHolder = new PagedListHolder<Book>(bookMap.values().asList() as List<Book>)
        bookPagedListHolder.pageSize = size
        bookPagedListHolder.page = page
        return bookPagedListHolder
    }
}
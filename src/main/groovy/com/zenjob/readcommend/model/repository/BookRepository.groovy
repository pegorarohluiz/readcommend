package com.zenjob.readcommend.model.repository

import com.zenjob.readcommend.model.Book
import org.springframework.beans.support.PagedListHolder

interface BookRepository {
    Optional<Book> findByAisn(String ISN)
    PagedListHolder<Book> getBooksPerPage(int size, int page)
}
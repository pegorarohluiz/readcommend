package com.zenjob.readcommend.model.repository

import com.zenjob.readcommend.model.Tenant
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface TenantRepository extends CrudRepository<Tenant, String>{

}
package com.zenjob.readcommend.model.repository

import com.zenjob.readcommend.model.User
import org.springframework.data.repository.CrudRepository

interface UserRepository extends CrudRepository<User, UUID>{

}
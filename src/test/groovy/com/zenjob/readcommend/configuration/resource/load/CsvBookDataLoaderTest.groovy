package com.zenjob.readcommend.configuration.resource.load

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class CsvBookDataLoaderTest extends Specification {

    @Autowired
    CsvBookDataLoader csvBookDataLoader

    def 'Scenario Load Book Data from CSV in Resources path'() {
        given: "The loaded book data from the classpath"
        def books = csvBookDataLoader.bookData()
        expect: "The books being loaded"
        books.size() == 12
    }
}

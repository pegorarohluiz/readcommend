package com.zenjob.readcommend


import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class ApplicationTest extends Specification {

    def 'Startup the Application'() {
        String[] args = ["spring.profiles.active", "test,book-data-csv"]
        Application.main(args)
    }
}

package com.zenjob.readcommend.api.commend.controller

import com.zenjob.readcommend.api.commend.model.RecommendationVo
import com.zenjob.readcommend.api.user.controller.UserController
import com.zenjob.readcommend.model.User
import com.zenjob.readcommend.model.enums.Evaluation
import com.zenjob.readcommend.model.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

@SpringBootTest
class CommendControllerIntegrationTest extends Specification {

    public static final String INTEGRATION_TEST_TENANT = "integrationTestTenant"
    public static final String USERNAME = "mad.max"

    @Autowired
    CommendController commendController

    @Autowired
    UserController userController

    @Autowired
    UserRepository userRepository

    @Shared
    User createdUser

    void setup() {
        createdUser = userController.createUser(INTEGRATION_TEST_TENANT, USERNAME).body
    }

    def "Scenario 02 - ACCEPTED (202) - Add Recommendations to User"() {
        given: "The integration test Tenant"
        def tenantHeader = INTEGRATION_TEST_TENANT
        and : "The user UUID"
        def uuid = createdUser.uid
        and: "The recommendations from the UI"
        def recommendationVos = [new RecommendationVo("195391349", Evaluation.LIKED),
                               new RecommendationVo("195630858", Evaluation.LIKED),
                               new RecommendationVo("195648943", Evaluation.LIKED),
                               new RecommendationVo("196360331", Evaluation.LIKED),
                               new RecommendationVo("198280424", Evaluation.LIKED),
                               new RecommendationVo("198393342", Evaluation.LIKED)
        ] as Set<RecommendationVo>

        when: "controller is called"
        def response = commendController.addNewRecommendations(tenantHeader, uuid.toString(), recommendationVos)
        then: "The response should contain the status code "
        response.statusCode == HttpStatus.ACCEPTED
        and: "The user should contain the the recommendations for the users listed above"
        def user = userRepository.findById(uuid).get()
        recommendationVos.each { recommendation ->
            user.recommendations.find {recommendation.bookAisn == it.book.asin}.evaluation != null
        }
    }

    @Unroll
    def "Scenario 03 - OK (200) - Get Books for Recommendations in #page with #pageSize"() {
        given: "the tenant for integration test"
        def tenantHeader = INTEGRATION_TEST_TENANT
        and: "the user evaluates at least one book in the first page"
        def recommendationVos = [new RecommendationVo("195391349", Evaluation.LIKED),
                                 new RecommendationVo("195630858", Evaluation.LIKED),
                                 new RecommendationVo("195648943", Evaluation.LIKED),
                                 new RecommendationVo("196360331", Evaluation.LIKED),
                                 new RecommendationVo("198280424", Evaluation.LIKED),
                                 new RecommendationVo("198393342", Evaluation.LIKED)
        ] as Set<RecommendationVo>
        commendController.addNewRecommendations(tenantHeader, createdUser.uid.toString(), recommendationVos)
        when: "ask"
        def response = commendController.retrieveBooksForRecommendation(tenantHeader, createdUser.username, pageSize, page).body
        then: "should contain the page containing any evaluations previously loaded"
        response.pageList.any {it.evaluation != null}
        and: "if any not evaluated, null evaluation"
        response.pageList.any {it.evaluation == null}
        where:
        page << ["0", "1"]
        pageSize << ["5", "5"]
    }
}

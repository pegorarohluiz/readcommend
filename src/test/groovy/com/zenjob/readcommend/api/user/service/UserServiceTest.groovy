package com.zenjob.readcommend.api.user.service

import com.zenjob.readcommend.api.commend.model.RecommendationVo
import com.zenjob.readcommend.configuration.tenant.TenantService
import com.zenjob.readcommend.model.Book
import com.zenjob.readcommend.model.Recommendation
import com.zenjob.readcommend.model.Tenant
import com.zenjob.readcommend.model.User
import com.zenjob.readcommend.model.enums.Evaluation
import com.zenjob.readcommend.model.repository.BookRepository
import com.zenjob.readcommend.model.repository.UserRepository
import org.assertj.core.util.Sets
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification

import static java.util.Collections.emptySet

class UserServiceTest extends Specification {

    @Shared
    def userRepository = Mock(UserRepository)

    @Shared
    def bookRepository = Mock(BookRepository)

    @Shared
    def tenantService = Mock(TenantService)

    @Shared
    def userService = new DefaultUserService(userRepository, bookRepository, tenantService)


    @Ignore("for some reason the mock for TenantService is not working on the tenantService.useTenant(_ as String), throwing the Tenant not found InvalidParameterException")
    def "Scenario of Create new user success"() {
        given: "some random username"
        def someUsername = "wile.e.coyote"
        and: "unit test tenant"
        def unitTestTenantId = "unitTestTenantId"
        when: "call the service to create"
        def createdUser = userService.createNewUser(unitTestTenantId, someUsername)
        then : "the tenant service will create the new tenant"
        1 * tenantService.useTenant(_ as String) >> new Tenant(unitTestTenantId, emptySet())
        1 * tenantService.addUserToTenant(_ as User, _ as Tenant) >> _
        and: "the repository should save the user just one time"
        1 * userRepository.save(_ as User) >> _
        and: "the user should match this specs"
        createdUser.with {
            username == "wile.e.coyote"
            uid == UUID.nameUUIDFromBytes(someUsername.getBytes())
            recommendations.isEmpty()
        }
    }

    @Ignore("for some reason the mock for TenantService is not working on the tenantService.useTenant(_ as String), throwing the Tenant not found InvalidParameterException")
    def "Scenario of Get User from UUID success"() {
        given: "some uuid"
        def someUid = UUID.randomUUID()
        and: "unit test tenant"
        def unitTestTenantId = "unitTestTenantId"
        def unitTestTenant = new Tenant(unitTestTenantId, new HashSet<User>([new User(someUid, "wile.e.coyote", emptySet())]))
        when: "call the service to create"
        def createdUser = userService.getUser unitTestTenantId, someUid
        then : "the tenant service will create the new tenant"
        1 * tenantService.useTenant(_ as String) >> unitTestTenant
        and: "the repository should save the user just one time"
        1 * userRepository.findById(_ as UUID) >> Optional.of(new User(someUid, "wile.e.coyote", emptySet()))
        and: "the user should match this specs"
        createdUser.with {
            username == "wile.e.coyote"
            uid == someUid
            recommendations.isEmpty()
        }
    }

    @Ignore("for some reason the mock for TenantService is not working on the tenantService.useTenant(_ as String), throwing the Tenant not found InvalidParameterException")
    def "Scenario of saving the Book recommendations success"() {
        given: "some uuid"
        def someUid = UUID.randomUUID()
        and: "unit test tenant"
        def unitTestTenantId = "unitTestTenantId"
        def unitTestTenant = new Tenant(unitTestTenantId, emptySet())
        and: "some recommendations"
        def newRecommendationVos = Sets.newHashSet([new RecommendationVo("ISN0001", Evaluation.LIKED), new RecommendationVo("ISN0002", Evaluation.DISLIKED),
                                    new RecommendationVo("ISN0003", Evaluation.N_A)])
        when: "some new recommendations arrive from the UI, save to the user UID"
        userService.saveRecommendations unitTestTenantId, someUid, newRecommendationVos
        def user = userService.getUser(unitTestTenantId, someUid)
        then: "the repository should retrieve the user with emptySet"
        1 * userRepository.findById(_ as UUID) >> Optional.of(new User(someUid, "road.runner", emptySet()))
        and : "the tenant service will use the new tenant"
        1 * tenantService.useTenant(_ as String) >> unitTestTenant
        1 * tenantService.addUserToTenant(_ as User, _ as Tenant) >> _
        and: "the book repository finds all three books"
        3 * bookRepository.findByAisn(_ as String) >> Optional.of(new Book(_ as String, _ as String, _ as String, _ as String))
        and: "then it should save the user with recommendations"
        1 * userRepository.save(_ as User) >> new User(someUid, "wile.e.coyote", newRecommendationVos as Set<Recommendation>)
        1 * userRepository.findById(_ as UUID) >> Optional.of(new User(someUid, "wile.e.coyote", newRecommendationVos as Set<Recommendation>))
        and: "user should contain three book recommendations"
        user.with {
            recommendations.size() == 3
        }
    }
}

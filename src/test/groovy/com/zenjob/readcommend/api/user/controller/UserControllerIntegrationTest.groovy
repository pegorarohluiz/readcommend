package com.zenjob.readcommend.api.user.controller

import com.zenjob.readcommend.api.commend.controller.CommendController
import com.zenjob.readcommend.model.User
import com.zenjob.readcommend.model.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest
class UserControllerIntegrationTest extends Specification {

    public static final String INTEGRATION_TEST_TENANT = "integrationTestTenant"
    public static final String USERNAME = "mad.max"

    @Autowired
    CommendController commendController

    @Autowired
    UserController userController

    @Autowired
    UserRepository userRepository

    @Shared
    User createdUser

    void "Scenario 01 - OK (200) - Create User with success"() {
        when: "create the user"
        createdUser = userController.createUser(INTEGRATION_TEST_TENANT, USERNAME).body
        then: "the user should have the information"
        createdUser.uid == UUID.nameUUIDFromBytes(USERNAME.bytes)
        createdUser.recommendations.isEmpty()
        createdUser.username == USERNAME
    }

}

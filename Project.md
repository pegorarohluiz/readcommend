# Book Review

## Task
Provide a simple book recommendation service which is usable via a REST API. It needs to be possible to define a new user, who will then be provided with 20 book recommendations. For a recommendation of a user feedback can be provided. The feedback can either be "liked the book", "disliked the book" or "not interested".
Requirements:

    Users are identified by their username.
    The list of recommendations should contain exactly 20 entries if possible.
    The code should be tested as appropriated.
    
## Proposal
### FrontEnd
Base Project is a SPA using Grails and React.
 1. Registration form with username only.
 1. Call the back-end of creating tenant and user, 
 1. Retrieve list the book to be recommended.
 1. Pageable list with items
 1. Add Actions to buttons
 1. Add API Call to update the book review given tenant, user, book and recommendation.
 1. Add filter for Title/Author/Genre

Prototype of List
In the list there should be listed like this

| Title |  Author | Genre | Interest | 
| ----- | ------- | ----- | --------|
| Art of War| Sun Tzu | War | [ Liked ] [ Disliked ] [ Not Interested ] |
  
List paging in the default 20 entries per page as requested, but changeable.

> Regards: This should implement one good looking CSS library, I am using the default for React, to develop fast.


### BackEnd
Base Project is a Spring Boot Multi-Tenant aware and scalable using Gradle and Groovy.
 1. Create Model for Tenant, User, Recommendation
 1. Create Tenant, User and Recommendation Repositories using Reactive MongoDB
 1. Load Book Source into application, and set to default Tenant, given the CSV provided.
 1. REST API for create user, receiveing the Tenant with X-Tenant-ID Http Header, the default being "latest"
 1. REST API retrieving the Book pageable List from the datasource.
 1. REST API creating single Recommendation
 
> The tenant should not be a Document containing the information, it should be separated schemas and collections, I am going this way to be faster.
 